<?php

/**
 * Si inexistante, on créée la table SQL "commissions" après l'activation du thème
 */

global $wpdb;
$charset_collate = $wpdb->get_charset_collate();

$sql = "CREATE TABLE IF NOT EXISTS Remise(
  id_remise INT AUTO_INCREMENT,
  taux DECIMAL(2,2),
  PRIMARY KEY(id_remise)
) $charset_collate;";

$sql2 = "CREATE TABLE IF NOT EXISTS Produit(
  id_produit INT AUTO_INCREMENT,
  description TEXT,
  prix DECIMAL(15,2) NOT NULL,
  image VARCHAR(255),
  nom VARCHAR(255) NOT NULL,
  promotion BOOL,
  id_remise INT NOT NULL,
  PRIMARY KEY(id_produit),
  FOREIGN KEY(id_remise) REFERENCES Remise(id_remise)
)$charset_collate;";



require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

dbDelta($sql2);
dbDelta($sql);
?>