<?php

/**
 * Plugin Name: mon 1er plugin
 * Version: 1.0.0
 * Author Name: Guillaume Bailly
 * Author: Guillaume Bailly
 * Description: C'est un plugin de test
 */
/*//supprime le menu article
function undefined_remove_menu_page (){
  // permet de supprimer le menu article
  remove_menu_page('edit.php');
}
add_action('admin_menu','undefined_remove_menu_page');
 */
include('DBcreation.php');
include('traitementDonnees.php');


class MonExampleClass
{
  public function __construct()
  {
    add_action('admin_menu', [$this, 'admin_add_menu']);
  }

  public function admin_add_menu()
  {
    add_menu_page(
      __('Mon titre', 'mon-titre'), // titre de la page
      __('Mon menu', 'mon-menu'), // libellé du menu
      'manage_options',
      'mon-menu',
      [$this, 'load_mon_menu'],
    );
  }
  // input box :definir url de l'image du produit
  // input box pour definir le prix actuel
  // input pox pour definir le prix apres remises
  // zone de saisie pour une description

  public function load_mon_menu()
  { ?>

    <h1> Voici mon menu</h1>

    <form action="" id="form" method="POST">
      <div>
        <input type="text" name="lien" placeholder="lien de l'image">
      </div>
      <div>
        <input type="number" name="prix_final" placeholder="prix apres reduction" />
      </div>
      <div>
        <input type="number" name="prix_reel" placeholder="prix reel" />
      </div>
      <div>
        <textarea name="description" cols="30" rows="10" placeholder="description du produit"></textarea>
      </div>
      <input type="submit" value="envoyer" />
    </form>

<?php
  }
}
new MonExampleClass();
